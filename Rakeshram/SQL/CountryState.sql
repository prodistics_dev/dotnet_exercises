CREATE TABLE [dbo].[Country](
	[Id] [int] IDENTITY (1,1) PRIMARY KEY,
	[countryName] [varchar](50) NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[State](
	[Id] [int] IDENTITY (1,1) PRIMARY KEY,
	[stateName] [varchar](50) NULL,
	[countryId] [int] NOT NULL
	CONSTRAINT FK_Country FOREIGN KEY (CountryId)
    REFERENCES [dbo].[Country] (Id)
) ON [PRIMARY]
GO

INSERT INTO [dbo].[Country](countryName)
VALUES ('INDIA');
GO

INSERT INTO [dbo].[Country](countryName)
VALUES ('NEPAL');
GO

INSERT INTO [dbo].[Country](countryName)
VALUES ('AMERICA');
GO

INSERT INTO [dbo].[Country](countryName)
VALUES ('DUBAI');
GO

INSERT INTO [dbo].[Country](countryName)
VALUES ('CHINA');
GO

SELECT * FROM [dbo].[Country]
GO

INSERT INTO [dbo].[State](stateName,countryId)
VALUES ('TAMILNADU',1);
GO

INSERT INTO [dbo].[State](stateName,countryId)
VALUES ('KERALA',1);
GO

INSERT INTO [dbo].[State](stateName,countryId)
VALUES ('TEXAS',3);
GO

SELECT * FROM [dbo].[State]
GO
