﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Forms;

namespace TestConnections
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.LoadCountries();

        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoadCountries()
        {
        
            string connetionString = null;
            SqlConnection cnn;
            SqlCommand cmd;
            SqlDataReader dataReader;
            connetionString = "Data Source=LAPTOP-SJ6PCT2A\\SQLSERVER;Initial Catalog=chennova;Integrated Security=SSPI";
            cnn = new SqlConnection(connetionString);
            string sql = "SELECT * FROM dbo.Country";
            try
            {
                this.CountryComboBox.Items.Clear();
                //  Open the connection.
                cnn.Open();
                cmd = new SqlCommand(sql, cnn);
                dataReader = cmd.ExecuteReader();
                DataTable countryDataTable = new DataTable();        
                countryDataTable.Load(dataReader);
                this.CountryComboBox.DisplayMember = "countryName";
                this.CountryComboBox.ValueMember = "Id";
                this.CountryComboBox.DataSource = countryDataTable;   
                cnn.Close();
             

            }
            catch (Exception ex)
            {
                MessageBox.Show("Can not open connection ! ");
            }

        }

        private void LoadStates()
        {
            string connetionString = null;
            SqlConnection cnn;
            SqlCommand cmd;
            SqlDataReader dataReader;
            connetionString = "Data Source=LAPTOP-SJ6PCT2A\\SQLSERVER;Initial Catalog=chennova;Integrated Security=SSPI";
            try
            {
                this.StateComboBox.DataSource = null;
                this.StateComboBox.Items.Clear();
                cnn = new SqlConnection(connetionString);
                cmd = new SqlCommand("SELECT * FROM dbo.State WHERE countryId = @ID", cnn);
                cmd.Parameters.AddWithValue("@ID", this.CountryComboBox.SelectedValue);
                //  Open the connection.
                cnn.Open();
                dataReader = cmd.ExecuteReader();
                DataTable dataTable = new DataTable();
                dataTable.Load(dataReader);
                this.StateComboBox.DisplayMember = "stateName";
                this.StateComboBox.ValueMember = "Id";
                this.StateComboBox.DataSource = dataTable;
                cnn.Close();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Can not open connection ! ");
            }
        }

       
        private void CountryComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.LoadStates();
        }
    }
}



