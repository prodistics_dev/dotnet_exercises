create table CountryMaster(
	ID int primary key identity(1,1),
	CountryName varchar(20),
	CreatedDtt datetime default(getdate())
	);

create table StateMaster(
	StateId int primary key identity(1,1),
	StateName varchar(20),
	CountryId int foreign key references Country(ID),
	CreatedDtt datetime default(getdate())
	);

insert into Country (CountryName) Values ('India');

insert into Country (CountryName) Values ('USA');

insert into Country (CountryName) Values ('UAE');

insert into Country (CountryName) Values ('SriLanka')

insert into Country (CountryName) Values ('China');

insert into StateName(State_Name,CountryName,CountryId) values ('TamilNadu','India',1)

insert into StateName(State_Name,CountryName,CountryId) values ('Mumbai','India',1)

insert into StateName(State_Name,CountryName,CountryId) values ('Delhi','India',1)

insert into StateName(State_Name,CountryName,CountryId) values ('California','USA',2)

insert into StateName(State_Name,CountryName,CountryId) values ('washington DC','USA',2)

insert into StateName(State_Name,CountryName,CountryId) values ('NewYork','USA',2)

insert into StateName(State_Name,CountryName,CountryId) values ('Abu Dhabi','UAE',3)

insert into StateName(State_Name,CountryName,CountryId) values ('Dubai','UAE',3)

insert into StateName(State_Name,CountryName,CountryId) values ('Sharjah','UAE',3)

insert into StateName(State_Name,CountryName,CountryId) values ('Jaffna','SriLanka',4)

insert into StateName(State_Name,CountryName,CountryId) values ('Mannar','SriLanka',4)

insert into StateName(State_Name,CountryName,CountryId) values ('Colombo','SriLanka',4)

insert into StateName(State_Name,CountryName,CountryId) values ('Hong Kong','China',5)

insert into StateName(State_Name,CountryName,CountryId) values ('Beijing','China',5)

insert into StateName(State_Name,CountryName,CountryId) values ('Shanghai','China',5)

