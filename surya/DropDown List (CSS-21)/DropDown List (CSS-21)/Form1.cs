﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace DropDown_List__CSS_21_
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            SqlConnection connect;
            String connectionstring = @"Data Source=surya;Initial Catalog=student;User ID=sa;Password=asusurya";
            connect = new SqlConnection(connectionstring);
            connect.Open();
            //MessageBox.Show(@"connection open");
            SqlCommand command = new SqlCommand("select * from CountryMaster", connect);
            SqlDataReader dataReader;
            dataReader = command.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(dataReader);
            comboCountry.DisplayMember = "CountryName";
            comboCountry.ValueMember = "ID";
            comboCountry.DataSource = dataTable;
            connect.Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboCountry.SelectedValue.ToString() != null)
            {
                SqlConnection connect;
                String connectionstring = @"Data Source=surya;Initial Catalog=student;User ID=sa;Password=asusurya";
                connect = new SqlConnection(connectionstring);
                SqlCommand command = new SqlCommand($"select * from StateMaster where CountryId={comboCountry.SelectedValue}", connect);
                connect.Open();
                SqlDataReader dataReader;
                dataReader = command.ExecuteReader();
                DataTable dataTable = new DataTable();
                dataTable.Load(dataReader);
                comboState.DisplayMember = "StateName";
                comboState.ValueMember = "StateId";
                comboState.DataSource = dataTable;
                connect.Close();
            }
        }
    }
}
