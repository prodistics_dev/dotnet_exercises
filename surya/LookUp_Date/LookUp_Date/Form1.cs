﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace LookUp_Date
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        
        public static string gender; 
        
        protected void button1_Click(object sender, EventArgs e)
        {
            SqlConnection connect;
            String connectionstring = @"Data Source=surya;Initial Catalog=student;User ID=sa;Password=asusurya";
            connect = new SqlConnection(connectionstring);
            connect.Open();
            MessageBox.Show(@"connection open");
            String query = "insert into Employee_Details(Id,Emp_Name,Email,Age,DOB,StateID,Gender,Administrator)" +
                "values ('" + this.E_ID.Text + "','" + this.E_Name.Text + "','" + this.E_Email.Text + "','" + this.E_Age.Text + "'," +
                "'" + this.dateTimePicker1.Value + "','" + this.comboBox1.SelectedValue + "','"+gender+"','"+this.checkBox1.Checked+"');";
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();
            MessageBox.Show("DATA SAVED!");
            var model = new EmployeeModel();
            model.EmpId=E_ID.Text;
            model.EmpName = E_Name.Text;
            model.EmpEmail = E_Email.Text;
            model.EmpAge = E_Age.Text;
            model.EmpDOB=Convert.ToString( dateTimePicker1.Value.ToShortDateString());
            model.EmpState = comboBox1.Text;
            model.EmpGender = gender;
            model.EmpAdmin = Convert.ToString(this.checkBox1.Checked);
            model.abc = checkBox1.Checked;
            this.Hide();
            Form2 form = new Form2(model);
            form.Show();
            connect.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            SqlConnection connect;
            String connectionstring = @"Data Source=surya;Initial Catalog=student;User ID=sa;Password=asusurya";
            connect = new SqlConnection(connectionstring);
            connect.Open();
            //MessageBox.Show(@"connection open");
            SqlCommand command = new SqlCommand("select State_Name,State_ID from State_Table", connect);
            SqlDataReader dataReader;
            dataReader = command.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("State_Name");
            dataTable.Columns.Add("State_ID");
            dataTable.Load(dataReader);
            comboBox1.DisplayMember = "State_Name";
            comboBox1.ValueMember = "State_ID";
            comboBox1.DataSource = dataTable;
            connect.Close();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            gender = "Male";
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            gender = "Female";
        }
        
    }
}
