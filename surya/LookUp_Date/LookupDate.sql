create table State_Table(
	State_ID int primary key identity(1,1),
	Country varchar(30),
	State_Name varchar(30),
	State_Code varchar(10)
	);
insert into State_Table (Country,State_Name,State_Code)
values ('INDIA','TAMILNADU','TN')

insert into State_Table (Country,State_Name,State_Code)
values ('INDIA','KERELA','KE')

insert into State_Table (Country,State_Name,State_Code)
values ('INDIA','KARNATAKA','KA')

insert into State_Table (Country,State_Name,State_Code)
values ('INDIA','DELHI','DL')

create table Employee_Details(
	Id int primary key,
	Emp_Name varchar(30),
	Email varchar(30),
	Age int,
	DOB datetime,
	StateId int references State_Table(State_ID),
	Gender varchar(10),
	Administrator BIT,
	CreatedDtt datetime default (getdate())
	);

