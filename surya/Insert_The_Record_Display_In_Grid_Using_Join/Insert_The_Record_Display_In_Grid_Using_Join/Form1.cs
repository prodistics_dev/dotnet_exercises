﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace Insert_The_Record_Display_In_Grid_Using_Join
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string connetionString;
            SqlConnection cnn;
            connetionString = @"Data Source=surya;Initial Catalog=student;User ID=sa;Password=surya";
            cnn = new SqlConnection(connetionString);
            cnn.Open();
            MessageBox.Show("Connection Open  !");
            SqlCommand cmd;
            SqlDataAdapter adapter = new SqlDataAdapter();
            String sql;
            String query;
            sql = "insert into register(Name,DOB,Mobileno,Age) values ('harish', '03-JUL-1995',9186595965,28)";
            query = "insert into marks(Mark1,Mark2,Mark3) values (87.5,95.2,66.5)";
            cmd = new SqlCommand(sql, cnn);
            adapter.InsertCommand = new SqlCommand(sql, cnn);
            adapter.InsertCommand.ExecuteNonQuery();
            adapter.InsertCommand = new SqlCommand(query, cnn);
            adapter.InsertCommand.ExecuteNonQuery();
            MessageBox.Show("!Inserted the data");
            cmd.Dispose();
            cnn.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string connectionString = @"Data Source=surya;Initial Catalog=student;User ID=sa;Password=surya";
            SqlConnection sqlcon = new SqlConnection(connectionString);
            sqlcon.Open();
            String query = "select register.Name,register.DOB,register.Mobileno,register.Age,marks.Mark1,marks.Mark2,marks.Mark3 from register " +
                "inner join marks on register.Registerno=marks.Registerno where register.Registerno = 1 ";
            SqlCommand cmd = new SqlCommand(query, sqlcon);
            DataTable dataTable = new DataTable();
            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
            dataAdapter.Fill(dataTable);
            dataGridView1.DataSource = dataTable;

        }
    }
}
