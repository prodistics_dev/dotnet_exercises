﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace datagrid_join_query
{
    public partial class Form1 : Form
    {
        SqlDataAdapter sda1, sda;
        DataTable dt;
        

        public Form1()
        {
            InitializeComponent();
        }

        private void btnResult_Click(object sender, EventArgs e)
        {
            SqlConnection Con = new SqlConnection(@"Data Source=LAPTOP-LNH0R6AT\SQLEXPRESS;Initial Catalog=Studentdetails;
                                                    User ID=sa;Password=sapass");

            string query = "Insert into MarkDetails(RollNo,Mark1,Mark2,Mark3)values('9','67.2','78.9','34.2')";
            SqlCommand cmd = new SqlCommand(query, Con);
            Con.Open();
            cmd.ExecuteNonQuery();
            Con.Close();

            sda1 = new SqlDataAdapter(@"update Markdetails set Mark2='96.7' 
                                        where RollNo='345'", Con);
            
            sda = new SqlDataAdapter(@"select StudentInfo.Name,StudentInfo.DateOfBirth,MarkDetails.Mark2,MarkDetails.Mark3
                                       from StudentInfo
                                       Inner join MarkDetails on StudentInfo.RollNo=MarkDetails.RollNo", Con);


            dt = new DataTable();

            sda1.Fill(dt);
            sda.Fill(dt);
            dataGridView1.DataSource = dt;
            
        }
    }
}
