create database Studentdetails;

use Studentdetails;

create table StudentInfo(
	RollNo int primary key identity(1,2) NOT FOR REPLICATION NOT NULL,	
	Name varchar(20),
	DateOfBirth datetime,
	Age int,
	MobileNo bigint,
	CreatedDtt DateTime NOT NULL Default Getdate());


SET IDENTITY_INSERT Studentdetails.dbo.StudentInfo ON; 

Insert into StudentInfo(RollNo,Name,DateOfBirth,Age,MobileNo) values('731','kumaran','2017-07-19','23','6309861234');

Insert into StudentInfo(RollNo,Name,DateOfBirth,Age,MobileNo) values('2','Aadhi','2018-4-8','5','6784539874');

Insert into StudentInfo(RollNo,Name,DateOfBirth,Age,MobileNo) values('345','raj','1986-8-9','30','6302093841');
 

 
 create table MarkDetails(
		RollNo int primary key,
		Mark1 decimal,
		Mark2 decimal,
		mark3 decimal);



Insert into MarkDetails(RollNo,Mark1,Mark2,Mark3) values('731','60.2','50.7','45.6');

Insert into MarkDetails(RollNo,Mark1,Mark2,Mark3) values('2','70.5','80.9','90.8');

insert into MarkDetails(RollNo,Mark1,Mark2,Mark3) values('345','70.9','43.2','89.76');


