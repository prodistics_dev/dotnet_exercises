﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace windows_look_up
{
    public class PersonalInfoModel
    {
        public string Name { get; set; }
        public string DateOFBirth { get; set; }
        public string MobileNo { get; set; }
        public string StateID { get; set; }
        public bool Skills { get; set; }
        public string Gender { get; set; }
    }
}
