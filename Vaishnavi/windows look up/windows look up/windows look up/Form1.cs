﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace windows_look_up
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            fillcombo();
        }
      
        void fillcombo()
        {
            string constring = @"Data Source=LAPTOP-LNH0R6AT\SQLEXPRESS;Initial Catalog=EmployeeDetails;
                                               User ID = sa; Password = sapass";
            string sql = "select * from OfficialInfo";
            SqlConnection Cn = new SqlConnection(constring);
            SqlCommand cd = new SqlCommand(sql, Cn);
            SqlDataReader rd;
            try
            {
                Cn.Open();
                rd = cd.ExecuteReader();
                while(rd.Read())
                {
                    int sStateID = rd.GetInt32(3);
                    comboBox1.Items.Add(sStateID);
                }
                
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
            Cn.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            //string str = " ";
            //foreach (Control c in this.Controls)
           // {
            //    if (c is CheckBox)
              //  {
                //    CheckBox b = (CheckBox)c;
                  //  if (b.Checked)
                 //   {
                    //    str = b.Text + " " + str;
                    //}
               // }
           // }

            SqlConnection Con = new SqlConnection(@"Data Source=LAPTOP-LNH0R6AT\SQLEXPRESS;Initial Catalog=EmployeeDetails;
                                               User ID=sa;Password=sapass");
            Con.Open();
            SqlCommand cmd = new SqlCommand("Insert into PersonalInfo(NAME,DateOfBirth,MobileNo,State_ID,Gender,skills)values('" + txtNAME.Text + "','" + dateTimePicker1.Text + "','" + txtMobileNo.Text + "','" + comboBox1.Text + "', @Gender,'" + checkBox1.Checked+"')", Con);


            if (rbtnMale.Checked == true)
            {
                cmd.Parameters.AddWithValue("@Gender", "Male");
            }
            else
            {
                cmd.Parameters.AddWithValue("@Gender", "Female");
            }
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            sda.Fill(ds);
            Con.Close();
            MessageBox.Show("Insert record Successfully");
            var model = new PersonalInfoModel();
            model.Name = txtNAME.Text;
            model.DateOFBirth = dateTimePicker1.Text;
            model.MobileNo = txtMobileNo.Text;
            model.StateID = comboBox1.Text;
            model.Skills = checkBox1.Checked;
            model.Gender = (rbtnMale.Checked == true) ? rbtnMale.Text : rbtnFemale.Text;
            this.Hide();
            Form form3 = new Form3(model);
            form3.Show();

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
