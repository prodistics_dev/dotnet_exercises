﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace combobox
{
    public class ProductModel
    {
        public int MyProperty { get; set; }
        public int ProdId { get; set; }
        public string ProdName { get; set; }
        public string ProdManufacture { get; set; }
        public string ProdRating { get; set; }
        public string ProdBrands { get; set; }
        public string ProdComments { get; set; }
    }
}
