﻿namespace combobox
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.id = new System.Windows.Forms.Label();
            this.name = new System.Windows.Forms.Label();
            this.manufacture = new System.Windows.Forms.Label();
            this.rating = new System.Windows.Forms.Label();
            this.brands = new System.Windows.Forms.Label();
            this.comments = new System.Windows.Forms.Label();
            this.t1 = new System.Windows.Forms.TextBox();
            this.t2 = new System.Windows.Forms.TextBox();
            this.t3 = new System.Windows.Forms.TextBox();
            this.t4 = new System.Windows.Forms.TextBox();
            this.t5 = new System.Windows.Forms.TextBox();
            this.t6 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // id
            // 
            this.id.AutoSize = true;
            this.id.Location = new System.Drawing.Point(305, 52);
            this.id.Name = "id";
            this.id.Size = new System.Drawing.Size(71, 17);
            this.id.TabIndex = 0;
            this.id.Text = "display_id";
            // 
            // name
            // 
            this.name.AutoSize = true;
            this.name.Location = new System.Drawing.Point(305, 94);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(95, 17);
            this.name.TabIndex = 1;
            this.name.Text = "display_name";
            // 
            // manufacture
            // 
            this.manufacture.AutoSize = true;
            this.manufacture.Location = new System.Drawing.Point(305, 138);
            this.manufacture.Name = "manufacture";
            this.manufacture.Size = new System.Drawing.Size(139, 17);
            this.manufacture.TabIndex = 2;
            this.manufacture.Text = "display_manufacture";
            // 
            // rating
            // 
            this.rating.AutoSize = true;
            this.rating.Location = new System.Drawing.Point(305, 192);
            this.rating.Name = "rating";
            this.rating.Size = new System.Drawing.Size(96, 17);
            this.rating.TabIndex = 3;
            this.rating.Text = "display_rating";
            // 
            // brands
            // 
            this.brands.AutoSize = true;
            this.brands.Location = new System.Drawing.Point(305, 229);
            this.brands.Name = "brands";
            this.brands.Size = new System.Drawing.Size(104, 17);
            this.brands.TabIndex = 4;
            this.brands.Text = "display_brands";
            // 
            // comments
            // 
            this.comments.AutoSize = true;
            this.comments.Location = new System.Drawing.Point(305, 266);
            this.comments.Name = "comments";
            this.comments.Size = new System.Drawing.Size(124, 17);
            this.comments.TabIndex = 5;
            this.comments.Text = "display_comments";
            // 
            // t1
            // 
            this.t1.Location = new System.Drawing.Point(497, 52);
            this.t1.Name = "t1";
            this.t1.Size = new System.Drawing.Size(100, 22);
            this.t1.TabIndex = 6;
            // 
            // t2
            // 
            this.t2.Location = new System.Drawing.Point(497, 94);
            this.t2.Name = "t2";
            this.t2.Size = new System.Drawing.Size(100, 22);
            this.t2.TabIndex = 7;
            // 
            // t3
            // 
            this.t3.Location = new System.Drawing.Point(497, 138);
            this.t3.Name = "t3";
            this.t3.Size = new System.Drawing.Size(100, 22);
            this.t3.TabIndex = 8;
            // 
            // t4
            // 
            this.t4.Location = new System.Drawing.Point(497, 187);
            this.t4.Name = "t4";
            this.t4.Size = new System.Drawing.Size(100, 22);
            this.t4.TabIndex = 9;
            // 
            // t5
            // 
            this.t5.Location = new System.Drawing.Point(497, 224);
            this.t5.Name = "t5";
            this.t5.Size = new System.Drawing.Size(100, 22);
            this.t5.TabIndex = 10;
            // 
            // t6
            // 
            this.t6.Location = new System.Drawing.Point(497, 266);
            this.t6.Name = "t6";
            this.t6.Size = new System.Drawing.Size(100, 22);
            this.t6.TabIndex = 11;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(845, 533);
            this.Controls.Add(this.t6);
            this.Controls.Add(this.t5);
            this.Controls.Add(this.t4);
            this.Controls.Add(this.t3);
            this.Controls.Add(this.t2);
            this.Controls.Add(this.t1);
            this.Controls.Add(this.comments);
            this.Controls.Add(this.brands);
            this.Controls.Add(this.rating);
            this.Controls.Add(this.manufacture);
            this.Controls.Add(this.name);
            this.Controls.Add(this.id);
            this.Name = "Form2";
            this.Text = "Form2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label id;
        private System.Windows.Forms.Label name;
        private System.Windows.Forms.Label manufacture;
        private System.Windows.Forms.Label rating;
        private System.Windows.Forms.Label brands;
        private System.Windows.Forms.Label comments;
        private System.Windows.Forms.TextBox t1;
        private System.Windows.Forms.TextBox t2;
        private System.Windows.Forms.TextBox t3;
        private System.Windows.Forms.TextBox t4;
        private System.Windows.Forms.TextBox t5;
        private System.Windows.Forms.TextBox t6;
    }
}