﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace combobox
{
    public partial class Form2 : Form
    {
        public Form2(ProductModel model)
        {
            InitializeComponent();
            t1.Text = model.ProdId.ToString();
            t2.Text = model.ProdName;
            t3.Text = model.ProdManufacture;
            t4.Text = model.ProdRating;
            t5.Text = model.ProdBrands;
            t6.Text = model.ProdComments;

        }
    }
}
