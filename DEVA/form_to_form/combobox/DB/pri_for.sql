use products ;
CREATE TABLE snacks(
   ID   INT              NOT NULL,
   NAME VARCHAR (20)     NOT NULL,
   manufacture datetime              NOT NULL,
   rating float       
   PRIMARY KEY (ID)
);
CREATE TABLE cosmetics (
   ID          INT        NOT NULL,
   name       varchar(20) not null,
     manufacture DATETIME, 
   prod_id INT references snacks (ID),
   rating float
   PRIMARY KEY (ID)
);

insert into snacks values (1,'nabati','2020-07-12 19:20:10','4.5');
insert into snacks values (11,'lotte','2000-07-12 19:20:10','3.5');
insert into snacks values (111,'munch','2020-07-10 17:26:10','4.5');
insert into snacks values (1111,'pastry','2018-07-12 19:30:10','3.9');
insert into snacks values (11111,'pasta','2020-06-12 17:20:10','5.5');
insert into snacks values (3,'pepsi','2020-07-12 19:20:10','5.1');
select * from snacks;
select * from cosmetics;
insert into cosmetics values (22,'garnier','2022-06-12 19:20:10',1,'4.5');
insert into cosmetics values (222,'loreal','2020-06-12 17:20:10',11,'2.3');
insert into cosmetics values (33,'pantene','2019-07-12 16:20:10',111,'4.5');
insert into cosmetics values (225,'dove','2018-06-12 17:20:10',11111,'2.3');
insert into cosmetics values (383,'lakme','2017-06-12 16:20:10',1111,'4.5');
select * from cosmetics;

select sn.name as SnackName, sn.rating as SnacksRating, 
cosm.name as CosmeticsName, cosm.rating as CosmeticsRating from snacks sn
inner join cosmetics cosm on cosm.prod_id = sn.IDs
where sn.rating < 4.5 


intersect select prod_id from cosmetics where rating > 4.5
delete from cosmetics;