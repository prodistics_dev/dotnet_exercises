﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace grid
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void include(object sender, EventArgs e)
        {
            string connetionString;
            SqlConnection cnn;
            connetionString = @"Data Source=LAPTOP-BGBO985N;Initial Catalog=products;User ID=sa;Password=sapass";
            cnn = new SqlConnection(connetionString);
            cnn.Open();
            MessageBox.Show("Connection Open  !");
            SqlCommand cmd;
            SqlDataAdapter adapter = new SqlDataAdapter();
            String sql;
            sql = "insert into snacks (id,name,manufacture,rating) values(55,'pepsi','2022-06-12 19:20:10',5.7)";
            cmd = new SqlCommand(sql, cnn);
            adapter.InsertCommand = new SqlCommand(sql, cnn);
            adapter.InsertCommand.ExecuteNonQuery();
            MessageBox.Show("items added");
            cmd.Dispose();
            cnn.Close();

        }

        private void show(object sender, EventArgs e)
        {
            string connectionString = @"Data Source=LAPTOP-BGBO985N;Initial Catalog= products;User ID=sa;Password=sapass";
            using (SqlConnection sqlcon = new SqlConnection(connectionString))
            {
                sqlcon.Open();
                MessageBox.Show("displayimg product with same rating");
                SqlDataAdapter dataAdapter = new SqlDataAdapter("select sn.name as SnackName, sn.rating as SnacksRating, " +
                    " cosm.name as CosmeticsName, cosm.rating as CosmeticsRating from snacks sn " +
                    " inner join cosmetics cosm on cosm.prod_id = sn.ID where sn.rating < 4.5", sqlcon);
                DataTable dataTable = new DataTable();
                dataAdapter.Fill(dataTable);
                dataGridView1.DataSource = dataTable;
                sqlcon.Close();


            }
        }
    }
}
