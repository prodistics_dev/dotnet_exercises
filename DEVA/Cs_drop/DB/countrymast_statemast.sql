CREATE TABLE CountryMaster (
    Id int NOT NULL,
    CName varchar(255) NOT NULL,
    CreatedDtt datetime default (getdate())
    PRIMARY KEY (Id)
);
drop table CountryMaster;
CREATE TABLE StateMaster (
   StateId int NOT NULL,
	StateName varchar(255) NOT NULL,
	CountryId int foreign key references  CountryMaster(Id),
	CreatedDtt datetime default (getdate())

);
SELECT * FROM CountryMaster;
insert into  CountryMaster(Id,CName) values (1,'India');
insert into  CountryMaster (Id,CName)values (2,'Afghanistan');
insert into  CountryMaster (Id,CName) values (3,'Albania');
insert into  CountryMaster (Id,CName) values (4,'Canada');
insert into StateMaster (StateId,StateName,CountryId) values(11,'Tamilnadu',1);
insert into StateMaster (StateId,StateName,CountryId) values(12, 'kerala',1);
insert into StateMaster (StateId,StateName,CountryId) values(13, 'karnataka',1);
insert into StateMaster (StateId,StateName,CountryId) values(14, 'Andhra',1);
insert into StateMaster (StateId,StateName,CountryId) values(21, 'ghor',2);
insert into StateMaster (StateId,StateName,CountryId) values(22, 'farah',2);
insert into StateMaster (StateId,StateName,CountryId) values(23, 'kabul',2);
insert into StateMaster (StateId,StateName,CountryId) values(31, 'diber',3);
insert into StateMaster (StateId,StateName,CountryId) values(32, 'durres',3);
insert into StateMaster (StateId,StateName,CountryId) values(41, 'alberta',4);
insert into StateMaster(StateId,StateName,CountryId) values(42, 'nova',4);
insert into StateMaster (StateId,StateName,CountryId) values(43, 'columbia',4);
select * from StateMaster;

select getdate()