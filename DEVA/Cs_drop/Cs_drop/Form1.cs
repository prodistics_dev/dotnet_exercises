﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Cs_drop
{
    public partial class Form1 : Form
    {
        List<Country> countries = new List<Country>();
        List<State> states = new List<State>();
        public Form1()
        {
            InitializeComponent();
        }

        private void comboBox1_DropDown(object sender, EventArgs e)
        {
            

        }

        private void comboBox2_DropDown(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            SqlConnection conn = new Connection().GetConnection();
            conn.Open();
            SqlCommand country = new SqlCommand("select * from CountryMaster", conn);
            SqlDataReader dr = country.ExecuteReader();
            while (dr.Read())
            {
                comboBox1.Items.Add(dr["CName"]);
                countries.Add(new Country()
                {
                    Id = ((int)dr["Id"]),
                    CName = dr["CName"] as string
                });
            }

            conn.Close();

            conn.Open();
            SqlCommand state = new SqlCommand("select * from StateMaster", conn);
            SqlDataReader dr1 = state.ExecuteReader();
            while (dr1.Read())
            {
               comboBox2.Items.Add(dr1["StateName"]);
                //tring S_Name;

                states.Add(new State()
                {
                    StateId = ( (int)dr1["StateId"]),
                    StateName = dr1["StateName"] as string,
                    CountryId = ((int)dr1["Countryid"]),
                });
            }

            conn.Close();

        }
          private string [] GetStateById(int id)
        {
            return states.Where(line => line.CountryId == id).Select(l => l.StateName).ToArray();
        }

        private string[] GetCountryById(int id)
        {
            return countries.Where(line => line.Id == id).Select(l => l.CName).ToArray();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBox2.Items.Clear();
            int Id = countries[comboBox1.SelectedIndex].Id;
            foreach (string name in GetStateById(Id))
            {
                this.comboBox2.Items.Add(name);
            }
        }
    }

    [Serializable]
    class Country
    {
        public int Id { get; set; }
        public string CName { get; set; }
    }
    class State
    {

        public int CountryId { get; set; }
      //  public object C_Name { get; internal set; }
        public int StateId { get; set; }
        public string StateName{ get; set; }
    }
}



